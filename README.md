small project that translates .graphml files (structured in a correct manner) to a ST text.
For use in Tia portal for example.

An exmample graph is provided and looks like this:


![example graph](Example Graphml/state diagram crane.png)


Upon starting the program you may select a graph, and the output is put in your clipboard.
For this graph the output will be:

```
CASE #State OF
    0:  //Idle
        
        IF "PUSH_SENS_BLOCK" = 1 THEN
            #State := 21;
            #timer1.IN := FALSE;
        END_IF;
        ;
    1:  //Lowering
        
        
        #timer1(IN := TRUE,
                PT := T#3s);
        IF #timer1.Q THEN
            #State := 3;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_DOWN" = 1 THEN
            #State := 2;
            #timer1.IN := FALSE;
        END_IF;
        ;
    2:  //Closing
        
        
        #timer1(IN := TRUE,
                PT := T#0.5s);
        IF #timer1.Q THEN
            #State := 5;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_OPEN" = 0 THEN
            #State := 4;
            #timer1.IN := FALSE;
        END_IF;
        ;
    4:  //Ascending
        
        
        #timer1(IN := TRUE,
                PT := T#4s);
        IF #timer1.Q THEN
            #State := 6;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_UP" = 1 THEN
            #State := 7;
            #timer1.IN := FALSE;
        END_IF;
        ;
    7:  //Retracting
        
        
        #timer1(IN := TRUE,
                PT := T#2s);
        IF #timer1.Q THEN
            #State := 9;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_BACK" = 1 THEN
            #State := 8;
            #timer1.IN := FALSE;
        END_IF;
        ;
    8:  //Rotating clockwise
        
        
        #timer1(IN := TRUE,
                PT := T#4s);
        IF #timer1.Q THEN
            #State := 10;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_ROT_CLOCK" = 1 THEN
            #State := 11;
            #timer1.IN := FALSE;
        END_IF;
        ;
    11: //Lowering
        
        
        #timer1(IN := TRUE,
                PT := T#3s);
        IF #timer1.Q THEN
            #State := 12;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_DOWN" = 1 THEN
            #State := 13;
            #timer1.IN := FALSE;
        END_IF;
        ;
    13: //Opening
        
        
        #timer1(IN := TRUE,
                PT := T#0.5s);
        IF #timer1.Q THEN
            #State := 14;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_OPEN" = 1 THEN
            #State := 15;
            #timer1.IN := FALSE;
        END_IF;
        ;
    15: //Ascending
        
        
        #timer1(IN := TRUE,
                PT := T#4s);
        IF #timer1.Q THEN
            #State := 16;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_UP" = 1 THEN
            #State := 17;
            #timer1.IN := FALSE;
        END_IF;
        ;
    17: //Rotating counter clockwise
        
        
        #timer1(IN := TRUE,
                PT := T#4s);
        IF #timer1.Q THEN
            #State := 19;
            #timer1.IN := FALSE;
        END_IF;
        IF "CRANE_SENS_ROT_CCLOCK" = 1 THEN
            #State := 18;
            #timer1.IN := FALSE;
        END_IF;
        ;
    18: //Extending
        
        
        #timer1(IN := TRUE,
                PT := T#2s);
        IF #timer1.Q THEN
            #State := 20;
            #timer1.IN := FALSE;
        END_IF;
        ;
    21: //checking
        
        //STATION 2 READY 
        ;
    3:  //err: crane can't lower
        
        ;
    5:  //err: crane can't close
        
        ;
    6:  //err: crane can't go up
        
        ;
    9:  //err: can't retract
        
        ;
    10: //err: rotating clockwise
        
        ;
    12: //err: crane can't lower
        
        ;
    14: //err: crane can't open
        
        ;
    16: //err: crane can't go up
        
        ;
    19: //err: can't rotate counter clockwise
        
        ;
    20: //err: can't extend
        
        ;
END_CASE;

```