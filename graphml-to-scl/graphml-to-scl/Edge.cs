﻿using System;

namespace sommmen.graphml_to_scl
{
    class Edge : IComparable<Edge>
    {
        public int From;
        public int To;
        public string Text;
        public override string ToString()
        {
            return "From: " + From + "\tTo: " + To + "\t" + Text;
        }

        public int CompareTo(Edge other)
        {
            if (Text.Contains("time"))
                return -1;
            if (other.Text.Contains("time"))
                return 1;
            if (Text.Contains("time") && other.Text.Contains("time"))
                return 0;
            return this.From.CompareTo(other.From); //doesnt really matter.
        }
    }
}
