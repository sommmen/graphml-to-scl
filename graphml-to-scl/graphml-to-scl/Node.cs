﻿using System;
using System.Linq;

namespace sommmen.graphml_to_scl
{
    class Node : IComparable<Node>
    {
        public int Id;
        public string Text;
        public override string ToString()
        {
            return Id + "\t" + Text;
        }

        public int CompareTo(Node n) //pushes error states to the bottom.
        {
            if (Text.StartsWith("err:") && n.Text.StartsWith("err:"))
                return Id.CompareTo(n.Id);
            if (Text.StartsWith("err:"))
                return 1;
            if (n.Text.StartsWith("err:"))
                return -1;

            //removes illegal characters (like ') so it can be used with contains. (can be easier, but this is more meta(
            //if (new string(Text.Where(Char.IsLetter).ToArray()).Contains("err:"))
            //    return 1;
            //if (new string(n.Text.Where(Char.IsLetter).ToArray()).Contains("err:")) 
            //    return 0;


            //sorting based on text. not needed really.
            //if (Text.Contains("err:") && n.Text.Contains("err:"))
            //    return String.Compare(Text.Substring(3), n.Text.Substring(3), StringComparison.Ordinal);
            return Id.CompareTo(n.Id);
        }
    }
}
