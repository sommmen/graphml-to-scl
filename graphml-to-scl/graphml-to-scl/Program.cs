﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace sommmen.graphml_to_scl
{
    //if the code is dirty; i made this listering to Dj Paul Elstak.
    //also feeling around with linq a bit; that's why some is not pretty.
    class Program
    {
        [STAThread]
        static void Main()
        {
            Console.WriteLine("Pick an .graphml file.");
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "C:\\Users\\Dion\\Google Drive\\Control of Industrial Processes",
                Filter = "graphml files (*.graphml)|*.graphml",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            Console.WriteLine("Apply timers aswell? [Y/N]");
            var pressed = Console.ReadKey().Key.ToString().ToLower();


            var nodes = new List<Node>();
            var edges = new List<Edge>();
            var dialogResult = openFileDialog1.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                if (dialogResult == DialogResult.Cancel)
                    return;
                Main();
                return;
            }
            Stream myStream;
            if ((myStream = openFileDialog1.OpenFile()) != null)
            {
                using (myStream)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(myStream);

                    foreach (var xmlElement in doc.OfType<XmlElement>().First().OfType<XmlElement>().First(n => n.Name == "graph").OfType<XmlElement>())
                    {
                        if (xmlElement.Name == "node")
                        {
                            //get id from current node
                            var node = new Node();
                            int.TryParse(xmlElement.GetAttribute("id").Substring(1), out node.Id);
                            foreach (var element in xmlElement.ChildNodes.OfType<XmlElement>().Where(n => n.HasChildNodes).ToList())
                            {
                                node.Text =
                                    element.FirstChild.ChildNodes.Cast<XmlElement>()
                                        .First(n => n.Name == "y:NodeLabel")
                                        .InnerText;
                            }
                            nodes.Add(node);
                        }
                        if (xmlElement.Name == "edge")
                        {
                            var edge = new Edge();
                            int.TryParse(xmlElement.GetAttribute("source").Substring(1), out edge.From);
                            int.TryParse(xmlElement.GetAttribute("target").Substring(1), out edge.To);
                            foreach (var element in xmlElement.Cast<XmlElement>().Where(n => n.HasChildNodes))
                            {
                                var firstOrDefault = element.Cast<XmlElement>()
                                    .First(n => n.Name == "y:QuadCurveEdge")
                                    .ChildNodes.Cast<XmlElement>()
                                    .FirstOrDefault(n => n.Name == "y:EdgeLabel");
                                if (firstOrDefault != null)
                                    edge.Text = firstOrDefault
                                        .InnerText;
                                else
                                    edge.Text = "";

                                if (edge.From != edge.To && edge.Text != "") //some edges seem weird (points to itself and contains no text), this filters them out.
                                    edges.Add(edge);
                            }
                        }
                    }
                }
            }
            //edges.ForEach(Console.WriteLine);
            //Console.WriteLine();
            //nodes.ForEach(Console.WriteLine);
            Clipboard.SetText(GenerateStStatemachine(nodes, edges, pressed=="y"));
            Console.WriteLine("Copied statemachine to clipboard!");
            //Console.ReadKey();
        }

        private static string GenerateStStatemachine(List<Node> nodes, List<Edge> edges, bool useTimers = true)
        {
            String result = "CASE #State OF\r\n";
            nodes.Sort();
            edges.Sort();
            foreach (var node in nodes)
            {
                result += "\t" + node.Id + ":\t//" + node.Text+"\r\n\t\t\r\n";
                foreach (var edge in edges.Where(e => e.From == node.Id))
                {
                    var splitted = edge.Text.Split(':');
                    //splitted.ToList().ForEach(Console.WriteLine);
                    if (splitted.Length == 2)
                    {
                        if (splitted[0].Contains("time_out") && useTimers) //timers
                        {
                            splitted[1] = splitted[1].Trim(' ');
                            result +=
                                "\r\n\t#timer1(IN := TRUE,\r\n\t\tPT := T#" + splitted[1] +
                                ");\r\n\tIF #timer1.Q THEN\r\n\t#State := " + edge.To +
                                ";\r\n\t#timer1.IN := FALSE;\r\n\tEND_IF;";
                        }
                        else
                        {
                            result += "\tIF \"" + splitted[0] + "\"=" + splitted[1] + " THEN \r\n\t\t#State := " +
                                      edge.To + ";\r\n\t#timer1.IN := FALSE;\r\n\tEND_IF;\r\n"; //TODO check if this is also possible with resetting the timer
                        }
                    }
                    else
                        result += "\t//"+edge.Text+"\r\n";
                }
                result += ";\r\n";
            }
            result += "END_CASE;\r\n";



            return result;
        }
    }
}
